package web.converter;

import core.model.Pizza;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import web.dto.PizzaDto;

/**
 * Created by Ionut on 17/6/2017.
 */
@Component
public class PizzaConverter extends BaseConverter<Pizza, PizzaDto> {
    private static final Logger log = LoggerFactory.getLogger(PizzaConverter.class);

    @Override
    public PizzaDto convertModelToDto(Pizza pizza) {
        PizzaDto pizzaDto = PizzaDto.builder()
                .name(pizza.getName())
                .description(pizza.getDescription())
                .price(pizza.getPrice())
                .cuisine(pizza.getCuisine())
                .build();
        pizzaDto.setId(pizza.getId());
        return pizzaDto;
    }
}