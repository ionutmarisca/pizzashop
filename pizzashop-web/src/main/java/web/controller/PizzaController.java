package web.controller;

import core.model.Pizza;
import core.service.PizzaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import web.converter.PizzaConverter;
import web.dto.FilterDto;
import web.dto.PizzaDto;
import web.dto.PizzasDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ionut on 17/6/2017.
 */
@RestController
public class PizzaController {
    private static final Logger log = LoggerFactory.getLogger(PizzaController.class);

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private PizzaConverter pizzaConverter;

    @RequestMapping(value = "/pizzas", method = RequestMethod.POST)
    public Map<String, PizzaDto> createPizza(
            @RequestBody final Map<String, PizzaDto> pizzaDtoMap) {
        log.trace("createPizza: pizzaDtoMap={}", pizzaDtoMap);

        PizzaDto pizzaDto = pizzaDtoMap.get("pizza");
        Pizza pizza = pizzaService.createPizza(pizzaDto.getName(), pizzaDto.getDescription(), pizzaDto.getPrice(), pizzaDto.getCuisine());

        Map<String, PizzaDto> result = new HashMap<>();
        result.put("pizza", pizzaConverter.convertModelToDto(pizza));

        log.trace("createPizza: result={}", result);

        return result;
    }

    @RequestMapping(value = "/pizzafilter", method = RequestMethod.POST)
    public PizzasDto filterPizza(
            @RequestBody final Map<String, FilterDto[]> filtersDtoMap) {
        boolean filterByPrice = false;
        float filterByPriceValue = 0;
        boolean filterCuisine = false;
        String filterCuisineValue = "";

        log.trace("filterPizza: filtersDtoMap={}", filtersDtoMap);

        FilterDto[] filtersDto = filtersDtoMap.get("filters");

        for(int i=0; i<filtersDto.length; i++) {
            if(filtersDto[i].getFilterType().equals("priceLessThan")) {
                filterByPrice = true;
                filterByPriceValue = Float.parseFloat(filtersDto[i].getFilterValue());
            }
            else {
                if(filtersDto[i].getFilterType().equals("cuisine")) {
                    filterCuisine = true;
                    filterCuisineValue = filtersDto[i].getFilterValue();
                }
            }
        }

        List<Pizza> filteredList = pizzaService.filterPizza(filterByPrice, filterByPriceValue, filterCuisine, filterCuisineValue);
        log.trace("filterPizza: result={}", filteredList);
        return new PizzasDto(pizzaConverter.convertModelsToDtos(filteredList));
    }
}
