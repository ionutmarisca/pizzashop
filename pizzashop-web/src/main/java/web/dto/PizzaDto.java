package web.dto;

import core.model.Pizza;
import lombok.*;

/**
 * Created by Ionut on 17/6/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PizzaDto extends BaseDto {
    private String name;
    private String description;
    private float price;
    private Pizza.Cuisine cuisine;

    @Override
    public String toString() {
        return "PizzaDto{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price + + '\'' +
                ", cuisine='" + cuisine +
                "'} " + super.toString();
    }
}