package web.dto;

import core.model.Pizza;
import lombok.*;

/**
 * Created by Ionut on 18/6/2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class FilterDto extends BaseDto {
    private String filterType;
    private String filterValue;
    @Override
    public String toString() {
        return "FilterDto{" +
                "filterType='" + filterType + '\'' +
                ", filterValue='" + filterValue +
                "'} " + super.toString();
    }
}