package core.service;

import core.model.Pizza;

import java.util.List;

/**
 * Created by Ionut on 17/6/2017.
 */
public interface PizzaService {
    Pizza createPizza(String name, String description, float price, Pizza.Cuisine cuisine);
    List<Pizza> filterPizza(boolean filterByPrice, float filterByPriceValue, boolean filterCuisine, String filterCuisineValue);
}
