package core.service;

import core.model.Pizza;
import core.repository.PizzaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ionut on 17/6/2017.
 */
@Service
public class PizzaServiceImpl implements PizzaService {
    private static final Logger log = LoggerFactory.getLogger(PizzaServiceImpl.class);

    @Autowired
    private PizzaRepository pizzaRepository;

    @Override
    public Pizza createPizza(String name, String description, float price, Pizza.Cuisine cuisine) {
        log.trace("createPizza: name={}, description={}, price={}, cuisine={}", name, description, price, cuisine);

        Pizza pizza = Pizza.builder()
                .name(name)
                .description(description)
                .price(price)
                .cuisine(cuisine)
                .build();
        pizza = pizzaRepository.save(pizza);

        log.trace("createPizza: pizza={}", pizza);

        return pizza;
    }

    @Override
    public List<Pizza> filterPizza(boolean filterByPrice, float filterByPriceValue, boolean filterCuisine, String filterCuisineValue) {
        log.trace("filterPizza: filterByPrice={}, filterByPriceValue={}, filterCuisine={}, filterCuisineValue={}", filterByPrice, filterByPriceValue, filterCuisine, filterCuisineValue);

        List<Pizza> pizzas = pizzaRepository.findAll();

        if(filterByPrice) {
            pizzas = pizzas.stream().filter(p -> p.getPrice()<filterByPriceValue).collect(Collectors.toList());
        }
        if(filterCuisine) {
            pizzas = pizzas.stream().filter(p -> p.getCuisine().toString().toLowerCase().equals(filterCuisineValue.toLowerCase())).collect(Collectors.toList());
        }

        log.trace("filterPizza: pizzas={}", pizzas);
        return pizzas;
    }
}
