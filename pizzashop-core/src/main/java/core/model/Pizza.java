package core.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Ionut on 17/6/2017.
 */
@Entity
@Table(name = "pizza")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pizza extends BaseEntity<Long> {
    public enum Cuisine { MEDITERRANEAN, ORIENTAL };
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "price", nullable = false)
    private float price;
    @Column(name = "cuisine", nullable = false)
    @Enumerated(EnumType.STRING)
    private Cuisine cuisine;
}
