package core.repository;

import core.model.Pizza;

/**
 * Created by Ionut on 17/6/2017.
 */

public interface PizzaRepository extends MainRepository<Pizza, Long> {}